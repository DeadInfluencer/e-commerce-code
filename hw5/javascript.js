function clickedButton() {
    alert('You clicked me!');
}

function backgroundColor(color) {
    document.body.style.backgroundColor = color;
}

function changeFont() {
    var text = document.getElementById('changeFont');

    text.style.fontFamily = 'Chiller';
    text.style.fontSize = '22pt';
    text.style.color = 'blue';
}

function textMouseOver(elementID) {
    var element = document.getElementById(elementID);

    element.innerHTML = "Get Off Me!";
}

function textMouseOut(elementID) {
    var element = document.getElementById(elementID);

    element.innerHTML = "Rollover me!";
}

function makeUpperCase(elementID) {
    var element = document.getElementById(elementID);
    var elementText = element.value.toUpperCase();

    element.value = elementText;
}

function doTheMath() {
    var op1 = parseInt(document.getElementById('math1').value);
    var op2 = parseInt(document.getElementById('math2').value);

    document.getElementById('mathResult').innerHTML = op1 + op2;
    //id handle the case where a number is not entered, but im too lazy. it's a feature
}

function change1(elementID) {
    document.getElementById(elementID).src = "img/kyoko.gif";
}

function change2(elementID) {
    document.getElementById(elementID).src = "img/shiro.gif";
}

function changeDef(elementID) {
    document.getElementById(elementID).src = "img/filo.gif";
}